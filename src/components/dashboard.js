import React, { useState, useEffect } from 'react';
import Navbar from '../components/navbar';
import { Container, Row, Col, ListGroup, Table, Button,Spinner } from 'react-bootstrap';
import { Speedometer, FileEarmarkFill,FolderFill,PeopleFill } from 'react-bootstrap-icons'
import axios from 'axios';

function Dashboard(props) {
    const [pageData, setPage] = useState([]);
    const [userData, setUser] = useState([]);
    const [isLoading,setLoading] = useState(true);
    const [messagePage,setMessagePage] = useState("")
    const [messageUser,setMessageUser]  =useState("")

    const dashboard = () => {
        props.history.push("/dashboard")
    }
    const page = () => {
        props.history.push("/pages")
    }
    const category = () => {
        props.history.push("/category")
    }
    const user = () => {
        props.history.push("/users")
    }
    return (
        <React.Fragment>
            <Navbar />
            <Container className='mt-4'>
                <Row>
                    <Col md={4}>
                        <ListGroup defaultActiveKey="#link1">
                            <ListGroup.Item action active onClick={dashboard}><Speedometer></Speedometer> Dashboard</ListGroup.Item>
                            <ListGroup.Item action onClick={page}><FileEarmarkFill></FileEarmarkFill> Pages</ListGroup.Item>
                            <ListGroup.Item action onClick={category}><FolderFill></FolderFill> Category</ListGroup.Item>
                            <ListGroup.Item action onClick={user}><PeopleFill></PeopleFill> Users</ListGroup.Item>
                        </ListGroup>
                    </Col>
                    <Col md={8}>
                        <Row>
                            <Col md={6}>
                                <span className="page-header" style={{ fontSize: "35px", color: "#1995dc" }}>
                                    <Speedometer></Speedometer> Dashboard
                                </span>
                            </Col>
                            <Col md={6}>
                            </Col>
                        </Row><hr />
                        <h4 className="lead" style={{ color: "#1995dc" }}><b>Latest Pages</b></h4>
                        <hr />
                        <h4 className="lead" style={{ color: "#1995dc" }}><b>Latest Users</b></h4>
                           </Col>
                </Row>
            </Container>
        </React.Fragment>
    )
}
export default Dashboard;