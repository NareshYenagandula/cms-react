import React, { useState, useEffect } from "react";
import Navbar from "../components/navbar";
import {
  Container,
  Row,
  Col,
  ListGroup,
  Button,
  Breadcrumb,
  Table,
  Alert,
  Spinner,
  Modal,
} from "react-bootstrap";
import {
  Speedometer,
  FileEarmarkFill,
  FolderFill,
  PeopleFill,
  TrashFill,
  PencilSquare,
} from "react-bootstrap-icons";
import { Link } from "react-router-dom";
import axios from "axios";
import ReactPaginate from "react-paginate";

function Page(props) {
  const [pageData, setData] = useState([]);
  const [message, setMessage] = useState();

  const dashboard = () => {
    props.history.push("/dashboard");
  };
  const page = () => {
    props.history.push("/pages");
  };
  const category = () => {
    props.history.push("/category");
  };
  const user = () => {
    props.history.push("/users");
  };
  return (
    <React.Fragment>
      <Navbar />
      <Container className="mt-4">
        <Row>
          <Col md={4}>
            <ListGroup defaultActiveKey="#link1">
              <ListGroup.Item action onClick={dashboard}>
                <Speedometer></Speedometer> Dashboard
              </ListGroup.Item>
              <ListGroup.Item action active onClick={page}>
                <FileEarmarkFill></FileEarmarkFill> Pages
              </ListGroup.Item>
              <ListGroup.Item action onClick={category}>
                <FolderFill></FolderFill> Category
              </ListGroup.Item>
              <ListGroup.Item action onClick={user}>
                <PeopleFill></PeopleFill> Users
              </ListGroup.Item>
            </ListGroup>
          </Col>
          <Col md={8} className="mt-4">
            <Row>
              <Col md={6}>
                <span
                  className="page-header"
                  style={{ fontSize: "35px", color: "#1995dc" }}
                >
                  <FileEarmarkFill></FileEarmarkFill> Pages
                </span>
              </Col>
              <Col md={6}>
                <div style={{ float: "right" }}>
                  <Link to="/pages/add">
                    <Button variant="outline-primary">
                      <b>New</b>
                    </Button>
                  </Link>
                </div>
              </Col>
            </Row>
            <hr />
            <Breadcrumb>
              <Breadcrumb.Item href="#">Dashboard</Breadcrumb.Item>
              <Breadcrumb.Item active>Pages</Breadcrumb.Item>
            </Breadcrumb>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
}
export default Page;
