import React, { useState } from 'react';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';

function Login() {
    const [loginData, setLoginData] = useState({ email: '', password: '' })
    const [error, setError] = useState({ emailErrorMessage: '', passwordErrorMessage: '' })

    return (
        <div style={{ paddingTop: "10%" }}>
            <Container>
                <Row>
                    <Col md={4}></Col>
                    <Col md={4} className="mt-4">
                        <Card style={{ borderRadius:"10px",boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Form>
                                <Card.Body>
                                    <Card.Title className=" text-center p-2">DCX CMS</Card.Title>
                                    <Form.Group>
                                        <Form.Control
                                            type="email"
                                            placeholder="Enter your Email"
                                            isInvalid={!!error.emailErrorMessage}
                                            autoFocus
                                            onChange={e => setLoginData({ ...loginData, email: e.target.value })}
                                        />
                                        <Form.Control.Feedback type="invalid">{error.emailErrorMessage}</Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control
                                            type="password"
                                            placeholder="Enter Password"
                                            isInvalid={!!error.passwordErrorMessage}
                                            onChange={e => setLoginData({ ...loginData, password: e.target.value })}
                                        />
                                        <Form.Control.Feedback type="invalid">{error.passwordErrorMessage}</Form.Control.Feedback>
                                    </Form.Group>
                                    <Button block type="submit" style={{borderRadius:"20px"}}>SIGN IN</Button>
                                </Card.Body>
                            </Form>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Login;